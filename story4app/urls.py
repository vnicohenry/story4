from django.urls import path
from . import views

app_name = 'story4app'

urlpatterns = [
    path('', views.home, name='home'),
    path('aboutme/', views.aboutme, name='aboutme'),
    # dilanjutkan ...
]